package br.ucsal.bes20181.bd2.locadora.factory;


import br.ucsal.bes20181.bd2.locadora.persistence.AcessorioDAO;
import br.ucsal.bes20181.bd2.locadora.persistence.FabricanteDAO;
import br.ucsal.bes20181.bd2.locadora.persistence.GrupoDAO;
import br.ucsal.bes20181.bd2.locadora.persistence.impl.AcessorioDAOJdbc;
import br.ucsal.bes20181.bd2.locadora.persistence.impl.FabricanteDAOJdbc;
import br.ucsal.bes20181.bd2.locadora.persistence.impl.GrupoDAOJdbc;

public class DAOFactory {

	private static GrupoDAO grupoDAO = null;

	private static AcessorioDAO acessorioDAO = null;	
	
	private static FabricanteDAO fabricanteDAO = null;
	
	public static GrupoDAO getGrupoDAO() {
		if (grupoDAO == null) {
			grupoDAO = new GrupoDAOJdbc();
		}
		return grupoDAO;
	}
	
	public static AcessorioDAO getAcessorioDAO() {
		if(acessorioDAO == null) {
			acessorioDAO = new AcessorioDAOJdbc();
		}
			return acessorioDAO;
	}
	
	public static FabricanteDAO getfabricanteDAO() {
		if(fabricanteDAO == null) {
			fabricanteDAO = new FabricanteDAOJdbc();
		}
			return fabricanteDAO;
	}

}
