package br.ucsal.bes20181.bd2.locadora.business;

import java.sql.SQLException;
import java.util.List;

import br.ucsal.bes20181.bd2.locadora.domain.Acessorio;
import br.ucsal.bes20181.bd2.locadora.factory.DAOFactory;
import br.ucsal.bes20181.bd2.locadora.persistence.AcessorioDAO;

public class AcessorioBO {
	
private static AcessorioDAO acessorioDAO = DAOFactory.getAcessorioDAO();
	
	public static List<Acessorio> findAll() throws ClassNotFoundException, SQLException {
		return acessorioDAO.findAll();
	}

	public static void insert(Acessorio acessorio) throws ClassNotFoundException{
		validar(acessorio);
		acessorioDAO.insert(acessorio);
	}

	private static void validar(Acessorio acessorio) {
	}
	
}
