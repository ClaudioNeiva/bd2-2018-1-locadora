package br.ucsal.bes20181.bd2.locadora.tui;

import java.sql.SQLException;
import java.util.List;

import br.ucsal.bes20181.bd2.locadora.business.AcessorioBO;
import br.ucsal.bes20181.bd2.locadora.domain.Acessorio;


public class AcessorioTui {

	public static void listar() throws ClassNotFoundException, SQLException {

		List<Acessorio> acessorios = AcessorioBO.findAll();
		System.out.println("Acessorios:");
		for (Acessorio acessorio : acessorios) {
			System.out.println(acessorio);
		}

	}

	public static void insert(Acessorio acessorio) {
		try {
			AcessorioBO.insert(acessorio);
			System.out.println("Acessorio inserido");			
		}catch (Exception e) {
			System.out.println("Falha ao inserir acessorio");			
		}	

	}
	
}
