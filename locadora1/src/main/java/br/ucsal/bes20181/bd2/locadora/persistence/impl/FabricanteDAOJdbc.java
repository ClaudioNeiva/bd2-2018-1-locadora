package br.ucsal.bes20181.bd2.locadora.persistence.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20181.bd2.locadora.domain.Fabricante;
import br.ucsal.bes20181.bd2.locadora.persistence.FabricanteDAO;

public class FabricanteDAOJdbc implements FabricanteDAO{
	private static final String JDBC_DRIVER = "org.postgresql.Driver";

	private static final String SERVER_IP = "localhost";

	private static final Integer SERVER_PORT = 6543;

	private static final String DATABASE = "locadora";

	private static final String JDBC_URL = "jdbc:postgresql://" + SERVER_IP + ":" + SERVER_PORT + "/" + DATABASE;

	private static final String USER = "postgres";

	private static final String PASSWORD = "postgresql";
	
	
	
	public List<Fabricante> findAll() throws ClassNotFoundException, SQLException {
		List<Fabricante> fabricantes = new ArrayList<>();

		Connection connection = getConnection();

		String query = "select cpnj, nome, telefone, logradouro, n�mero, complemento, bairro, cidade, estado from fabricante";

		Statement statement = connection.createStatement();

		ResultSet resultSet = statement.executeQuery(query);

		while (resultSet.next()) {

			fabricantes.add(resultSetToFabricante(resultSet));
		}

		return fabricantes;
	}

	
	public Fabricante findByCnpj(Integer cnpj) {
		Fabricante fabricante = null;
		try(Connection connection = getConnection();) {
			String query = "select cnpj,nome,telefone,logradouro,numero,complemento,bairro,cidade,estado from fabricante where cnpj = ?";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1, cnpj);
			ResultSet resultSet = preparedStatement.executeQuery(query);
			while (resultSet.next()) {
				fabricante = resultSetToFabricante(resultSet);
			}
			preparedStatement.execute();
		} catch (SQLException | ClassNotFoundException e) {
			
		}
		return fabricante;
	}

	
	public void insert(Fabricante fabricante) throws ClassNotFoundException {
		
		try(Connection connection = getConnection();) {
			String query = "insert into fabricante (cnpj,nome,telefone,logradouro,numero,complemento,bairro,cidade,estado) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1, fabricante.getCnpj());
			preparedStatement.setString(2, fabricante.getNome());
			preparedStatement.setString(3, fabricante.getTelefone());
			preparedStatement.setString(4, fabricante.getLogradouro());
			preparedStatement.setInt(5, fabricante.getNumero());
			preparedStatement.setString(6, fabricante.getComplemento());
			preparedStatement.setString(7, fabricante.getBairro());
			preparedStatement.setString(8, fabricante.getCidade());
			preparedStatement.setString(9, fabricante.getEstado());
			preparedStatement.execute();
		} catch (SQLException e) {
			
		}
		
	}

	
	public void update(Fabricante fabricante) {
		try(Connection connection = getConnection();) {
			String query = "update fabricante set  nome = ? , telefone = ? , logradouro = ? , numero = ? , complemento = ? , bairro = ? , cidade = ? , estado = ? where cnpj = ?";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, fabricante.getNome());
			preparedStatement.setString(2, fabricante.getTelefone());
			preparedStatement.setString(3, fabricante.getLogradouro());
			preparedStatement.setInt(4, fabricante.getNumero());
			preparedStatement.setString(5, fabricante.getComplemento());
			preparedStatement.setString(6, fabricante.getBairro());
			preparedStatement.setString(7, fabricante.getCidade());
			preparedStatement.setString(8, fabricante.getEstado());
			preparedStatement.setInt(9, fabricante.getCnpj());
			preparedStatement.execute();
		} catch (SQLException | ClassNotFoundException e) {
			
		}
		
	}

	
	public void delete(Fabricante fabricante) {
		try(Connection connection = getConnection();) {
			String query = "delete fabricante where cnpj = ?";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1, fabricante.getCnpj());
		} catch (ClassNotFoundException | SQLException e) {
		}
		
		
	}

	
	public void delete(Integer cnpj) {
		try(Connection connection = getConnection();) {
			String query = "delete fabricante where cnpj = ?";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1, cnpj);
		} catch (ClassNotFoundException | SQLException e) {
		}
		
	}
	
	private static Fabricante resultSetToFabricante(ResultSet resultSet) throws SQLException {
		Integer cnpj = resultSet.getInt("cnpj");
		String nome = resultSet.getString("nome");
		String telefone = resultSet.getString("telefone");
		String logradouro = resultSet.getString("logradouro");
		Integer numero = resultSet.getInt("numero");
		String complemento = resultSet.getString("complemento");
		String bairro = resultSet.getString("bairro");
		String cidade = resultSet.getString("cidade");
		String estado = resultSet.getString("estado");
		return new Fabricante(cnpj,nome,telefone,logradouro,numero,complemento,bairro,cidade,estado);
	}
	
	private static Connection getConnection() throws ClassNotFoundException, SQLException {

		Class.forName(JDBC_DRIVER);

		return DriverManager.getConnection(JDBC_URL, USER, PASSWORD);

	}
}
