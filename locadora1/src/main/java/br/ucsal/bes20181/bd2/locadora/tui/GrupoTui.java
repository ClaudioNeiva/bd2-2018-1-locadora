package br.ucsal.bes20181.bd2.locadora.tui;

import java.sql.SQLException;
import java.util.List;

import br.ucsal.bes20181.bd2.locadora.business.GrupoBO;
import br.ucsal.bes20181.bd2.locadora.domain.Grupo;

public class GrupoTui {

	public static void listar() throws ClassNotFoundException, SQLException {

		List<Grupo> grupos = GrupoBO.findAll();

		System.out.println("Grupos:");
		for (Grupo grupo : grupos) {
			System.out.println(grupo);
		}

	}

	public static void insert(Grupo grupo) {
		try {
			GrupoBO.insert(grupo);
			System.out.println("Grupo inserido");			
		}catch (Exception e) {
			System.out.println("Falha ao inserir grupo");			
		}	

	}

}
