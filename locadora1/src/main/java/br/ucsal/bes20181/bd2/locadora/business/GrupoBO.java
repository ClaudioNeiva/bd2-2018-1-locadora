package br.ucsal.bes20181.bd2.locadora.business;

import java.sql.SQLException;
import java.util.List;

import br.ucsal.bes20181.bd2.locadora.domain.Grupo;
import br.ucsal.bes20181.bd2.locadora.factory.DAOFactory;
import br.ucsal.bes20181.bd2.locadora.persistence.GrupoDAO;

public class GrupoBO {

	private static GrupoDAO grupoDAO = DAOFactory.getGrupoDAO();
	
	public static List<Grupo> findAll() throws ClassNotFoundException, SQLException {
		return grupoDAO.findAll();
	}

	public static void insert(Grupo grupo) throws ClassNotFoundException{
		validar(grupo);
		grupoDAO.insert(grupo);
	}

	private static void validar(Grupo grupo) {
	}
	
}
