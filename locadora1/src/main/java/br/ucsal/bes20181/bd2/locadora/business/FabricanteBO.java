package br.ucsal.bes20181.bd2.locadora.business;

import java.sql.SQLException;
import java.util.List;

import br.ucsal.bes20181.bd2.locadora.domain.Fabricante;
import br.ucsal.bes20181.bd2.locadora.factory.DAOFactory;
import br.ucsal.bes20181.bd2.locadora.persistence.FabricanteDAO;

public class FabricanteBO {
	private static FabricanteDAO fabricanteDAO = DAOFactory.getfabricanteDAO();

	public static List<Fabricante> findAll() throws ClassNotFoundException, SQLException {
		return fabricanteDAO.findAll();
	}

	public static void insert(Fabricante fabricante) throws ClassNotFoundException {
		validar(fabricante);
		fabricanteDAO.insert(fabricante);
	}

	public static void update(Fabricante fabricante) throws ClassNotFoundException {
		fabricanteDAO.update(fabricante);
	}

	public static void delete(Fabricante fabricante) throws ClassNotFoundException {
		fabricanteDAO.delete(fabricante.getCnpj());
	}

	public static void FindByCnpj(Fabricante fabricante) throws ClassNotFoundException {
		fabricanteDAO.findByCnpj(fabricante.getCnpj());
	}

	private static void validar(Fabricante fabricante) {
	}

}
