package br.ucsal.bes20181.bd2.locadora.persistence;

import java.sql.SQLException;
import java.util.List;

import br.ucsal.bes20181.bd2.locadora.domain.Fabricante;

public interface FabricanteDAO {
	List<Fabricante> findAll() throws ClassNotFoundException, SQLException;

	Fabricante findByCnpj(Integer cnpj);

	void insert(Fabricante fabricante) throws ClassNotFoundException;

	void update(Fabricante fabricante);

	void delete(Fabricante fabricante);

	void delete(Integer cnpj);
	
}
