package br.ucsal.bes20181.bd2.locadora.persistence;

import java.sql.SQLException;
import java.util.List;

import br.ucsal.bes20181.bd2.locadora.domain.Acessorio;

public interface AcessorioDAO {
	
	List<Acessorio> findAll() throws ClassNotFoundException, SQLException;

	Acessorio findBySigla(String sigla) throws ClassNotFoundException;

	void insert(Acessorio acessorio) throws ClassNotFoundException;

	void update(Acessorio acessorio) throws ClassNotFoundException;

	void delete(Acessorio acessorio) throws ClassNotFoundException;

	void delete(String sigla) throws ClassNotFoundException;
}
