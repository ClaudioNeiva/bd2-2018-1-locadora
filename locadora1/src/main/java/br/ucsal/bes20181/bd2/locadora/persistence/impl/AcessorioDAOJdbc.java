package br.ucsal.bes20181.bd2.locadora.persistence.impl;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20181.bd2.locadora.domain.Acessorio;
import br.ucsal.bes20181.bd2.locadora.persistence.AcessorioDAO;

public class AcessorioDAOJdbc implements AcessorioDAO {

	private static final String JDBC_DRIVER = "org.postgresql.Driver";

	private static final String SERVER_IP = "localhost";

	private static final Integer SERVER_PORT = 6543;

	private static final String DATABASE = "locadora";

	private static final String JDBC_URL = "jdbc:postgresql://" + SERVER_IP + ":" + SERVER_PORT + "/" + DATABASE;

	private static final String USER = "postgres";

	private static final String PASSWORD = "postgresql";

	public List<Acessorio> findAll() throws ClassNotFoundException, SQLException {

		List<Acessorio> acessorios = new ArrayList<>();

		Connection connection = getConnection();
		String query = "select sigla, nome from acessorio";
		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);
		while (resultSet.next()) {
			acessorios.add(resultSetToAcessorio(resultSet));
		}

		return acessorios;
	}


	@Override
	public Acessorio findBySigla(String sigla) throws ClassNotFoundException {
				try (Connection connection = getConnection()){
					String query = "select sigla, nome from acessorio where sigla = ?";
					PreparedStatement preparedStatement = connection.prepareStatement(query);
					preparedStatement.setString(1, sigla);
					preparedStatement.execute();
				}  catch (SQLException ex) {
					throw new RuntimeException(ex);
				}
				return null;
	}

	@Override
	public void insert(Acessorio acessorio) throws ClassNotFoundException {
		try (Connection connection = getConnection()){
			String query = "insert into acessorio(sigla, nome) values (?, ?)";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, acessorio.getSigla());
			preparedStatement.setString(2, acessorio.getNome());
			preparedStatement.execute();
		}  catch (SQLException ex) {
			throw new RuntimeException(ex);
		}


	}

	@Override
	public void update(Acessorio acessorio) throws ClassNotFoundException {
		try (Connection connection = getConnection()){
			String query = "update acessorio set sigla = ?, nome = ? where sigla = ? ";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, acessorio.getSigla());
			preparedStatement.setString(2, acessorio.getNome());
			preparedStatement.setString(3, acessorio.getSigla());
			preparedStatement.execute();
		}  catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}


	@Override
	public void delete(Acessorio acessorio) throws ClassNotFoundException {
		try (Connection connection = getConnection()){
			String query = "delete from acessorio where sigla = ? and nome = ? ";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, acessorio.getSigla());
			preparedStatement.setString(2, acessorio.getNome());
			preparedStatement.execute();
		}  catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public void delete(String sigla) throws ClassNotFoundException {
		try (Connection connection = getConnection()){
			String query = "delete from acessorio where sigla = ?";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, sigla);
			preparedStatement.execute();
		}  catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	private static Acessorio resultSetToAcessorio(ResultSet resultSet) throws SQLException {
		String sigla = resultSet.getString("sigla");
		String nome = resultSet.getString("nome");
		return new Acessorio(sigla, nome);
	}

	private static Connection getConnection() throws ClassNotFoundException, SQLException {

		Class.forName(JDBC_DRIVER);

		return DriverManager.getConnection(JDBC_URL, USER, PASSWORD);

	}

}
