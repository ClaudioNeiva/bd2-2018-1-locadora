package br.ucsal.bes20181.bd2.locadora.tui;

import java.sql.SQLException;
import java.util.List;

import br.ucsal.bes20181.bd2.locadora.business.FabricanteBO;
import br.ucsal.bes20181.bd2.locadora.domain.Fabricante;

public class FabricanteTui {

	public static void listar() throws ClassNotFoundException, SQLException {

		List<Fabricante> Fabricantes = FabricanteBO.findAll();

		System.out.println("Fabricantes:");
		for (Fabricante Fabricante : Fabricantes) {
			System.out.println(Fabricante);
		}

	}

	public static void insert(Fabricante Fabricante) {
		try {
			FabricanteBO.insert(Fabricante);
			System.out.println("Fabricantes inserido");			
		}catch (Exception e) {
			System.out.println("Falha ao inserir Fabricantes");			
		}	

	}

}
