package br.ucsal.bes20181.bd2.locadora.tui;
import br.ucsal.bes20181.bd2.locadora.domain.Grupo;
import java.sql.SQLException;

public class Exemplo {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		
		GrupoTui.listar();
		Grupo gru = new Grupo(21, "show");
		GrupoTui.insert(gru);
		GrupoTui.listar();
	}
	
}
