package br.ucsal.bes20181.bd2.locadora.persistence.impl;
import java.sql.PreparedStatement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20181.bd2.locadora.domain.Grupo;
import br.ucsal.bes20181.bd2.locadora.persistence.GrupoDAO;

public class GrupoDAOJdbc implements GrupoDAO {

	private static final String JDBC_DRIVER = "org.postgresql.Driver";

	private static final String SERVER_IP = "localhost";

	private static final Integer SERVER_PORT = 6543;

	private static final String DATABASE = "locadora";

	private static final String JDBC_URL = "jdbc:postgresql://" + SERVER_IP + ":" + SERVER_PORT + "/" + DATABASE;

	private static final String USER = "postgres";

	private static final String PASSWORD = "postgresql";

	public List<Grupo> findAll() throws ClassNotFoundException, SQLException {

		List<Grupo> grupos = new ArrayList<>();

		// Conectar o banco de dados
		Connection connection = getConnection();

		// Recuperar os dados da tabela grupo
		String query = "select codigo, nome from grupo";

		Statement statement = connection.createStatement();

		ResultSet resultSet = statement.executeQuery(query);

		while (resultSet.next()) {
			// Instanciar um objeto da classe Grupo para cada linha da tabela
			// grupo
			grupos.add(resultSetToGrupo(resultSet));
		}

		return grupos;
	}


	@Override
	public Grupo findByCodigo(Integer codigo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insert(Grupo grupo) throws ClassNotFoundException {
		try {
			Connection connection = getConnection();
			String query = "insert into grupo (nome, codigo) values (?, ?)";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, grupo.getNome());
			preparedStatement.setInt(2, grupo.getCodigo());
			preparedStatement.execute();
			connection.close();
		}  catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		
	}

	@Override
	public void update(Grupo grupo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Grupo grupo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Integer codigo) {
		// TODO Auto-generated method stub
		
	}

	private static Grupo resultSetToGrupo(ResultSet resultSet) throws SQLException {
		Integer codigo = resultSet.getInt("codigo");
		String nome = resultSet.getString("nome");
		return new Grupo(codigo, nome);
	}

	private static Connection getConnection() throws ClassNotFoundException, SQLException {

		Class.forName(JDBC_DRIVER);

		return DriverManager.getConnection(JDBC_URL, USER, PASSWORD);

	}

}
