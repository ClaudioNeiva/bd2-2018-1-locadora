package br.ucsal.bes20181.bd2.locadora.persistence;

import java.sql.SQLException;
import java.util.List;

import br.ucsal.bes20181.bd2.locadora.domain.Grupo;

public interface GrupoDAO {

	List<Grupo> findAll() throws ClassNotFoundException, SQLException;

	Grupo findByCodigo(Integer codigo);

	void insert(Grupo grupo) throws ClassNotFoundException;

	void update(Grupo grupo);

	void delete(Grupo grupo);

	void delete(Integer codigo);

}
